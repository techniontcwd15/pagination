import { PaginationExamplePage } from './app.po';

describe('pagination-example App', () => {
  let page: PaginationExamplePage;

  beforeEach(() => {
    page = new PaginationExamplePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
