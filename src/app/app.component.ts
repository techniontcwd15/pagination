import { Component, OnInit } from '@angular/core';
import { RestService } from './rest.service';

interface Game {
  game:string,
  platform: string,
  release: string
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [RestService]
})
export class AppComponent implements OnInit {
  private page:number = 1;
  private games:Array<Game> = [];

  constructor(private rest:RestService) {}

   nextPage() {
     this.page += 1;
   }

   ngOnInit() {
     this.rest.getData().subscribe((games)=> {
       this.games = games;
     });
   }
 }
