import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs';

@Injectable()
export class RestService {

  constructor(private http:Http) { }

  getData() {
    return this.http.get('http://localhost/restcheck.php')
      .map(res=>res.json());
  }
}
